import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay

if __name__ == "__main__":
    true_y = pd.read_csv('data/test_values.csv', header=None).to_numpy().flatten()
    predict_y = pd.read_csv('data/predict_values.csv', header=None).to_numpy().flatten()
    IC = type('IdentityClassifier', (), {"predict": lambda i: i, "_estimator_type": "classifier"})
    cm = ConfusionMatrixDisplay.from_estimator(IC, true_y, predict_y, normalize='true', values_format='.2%')
    cm.figure_.savefig('data/plot.png')

    with open('data/metrics.txt', 'w') as metrics_file:
        print(confusion_matrix(true_y, predict_y), file=metrics_file)
        print(classification_report(true_y, predict_y), file=metrics_file)