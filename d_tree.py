import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split


if __name__ == '__main__':
    iris_data = pd.read_csv('data/iris.csv', header=None)
    train_x, test_x, train_y, test_y = train_test_split(iris_data.iloc[:, :-1], iris_data.iloc[:, -1], test_size=0.2)
    classifier = DecisionTreeClassifier()
    classifier.fit(train_x, train_y)
    pd.DataFrame(classifier.predict(test_x)).to_csv('data/predict_values.csv', index=False, header=False)
    pd.DataFrame(test_y).to_csv('data/test_values.csv', index=False, header=False)
