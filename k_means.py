import pandas as pd
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
import itertools
from sklearn.metrics import accuracy_score


def mapping(test_y, predicted_y):
    mappings = list(itertools.permutations(list(set(test_y)), 3))
    max_accuracy = -10000
    good_mapping = []
    for mapping in mappings:
        transformed_predicted_y = []
        for index_class in predicted_y:
            transformed_predicted_y.append(mapping[index_class])
        acc_score = accuracy_score(test_y, transformed_predicted_y)
        if (max_accuracy < acc_score):
            good_mapping = transformed_predicted_y
            max_accuracy = acc_score

    return good_mapping


if __name__ == "__main__":
    iris_data = pd.read_csv('data/iris.csv')
    train_X, test_X = train_test_split(iris_data, test_size=0.2)
    kmeans = KMeans(3)
    kmeans.fit(train_X[train_X.columns[:-1]].to_numpy())
    predicted_y = kmeans.predict(test_X[test_X.columns[:-1]].to_numpy())
    pd.DataFrame(mapping(test_X[test_X.columns[-1]].to_numpy(), predicted_y)).to_csv("data/predict_values.csv", index=False, header=False)
    pd.DataFrame(test_X[test_X.columns[-1]].to_numpy()).to_csv("data/test_values.csv", index=False, header=False)